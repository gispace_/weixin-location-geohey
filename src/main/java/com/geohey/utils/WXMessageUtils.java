package com.geohey.utils;


public class WXMessageUtils {

	public static String TO_USER_NAME = "ToUserName";
	public static String FROM_USER_NAME = "FromUserName";
	public static String MSG_TYPE = "MsgType";
	public static String EVENT = "Event";
	public static String EVENT_KEY = "EventKey";
	public static String X = "Longitude";
	public static String Y = "Latitude";
	public static String CREATE_TIME = "CreateTime";
	public static String CONTENT = "Content";

	public static String MSG_EVENT_LOCATION = "LOCATION";
	public static String MSG_EVENT_SUBSCRIBE = "subscribe";
	public static String MSG_EVENT_CLICK = "CLICK";

	public static String MSG_TYPE_TEXT = "text";
	public static String MSG_TYPE_EVENT = "event";

}
