package com.geohey.utils;

import java.nio.charset.Charset;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

public class JerseyUtils {

	public static String readStringAndClose(Response resp, Client client){
		return readStringAndClose(resp, client, null);
	}
	
	public static String readStringAndClose(Response resp, Client client, String charsetName){
		if(resp == null)
			return null;
		
		if(!resp.hasEntity()){
			if(client != null)
				client.close();
			return null;
		}
		
		byte[] bytes = resp.readEntity(byte[].class);
		
		if(client != null)
			client.close();
		
		charsetName = charsetName == null ? "UTF-8" : charsetName;
		
		Charset charset = null;
		try{
			charset = Charset.forName(charsetName);
		} catch(Exception ex){
		}
		
		return new String(bytes, charset);
	}
}
