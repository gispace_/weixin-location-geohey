package com.geohey.wx;

public class WXUserInfo {

	private String openId;
	private String nickName;
	private String thumbnail;

	public WXUserInfo() {
	}

	public WXUserInfo(String openId, String nickName, String thumbnail) {
		this.openId = openId;
		this.nickName = nickName;
		this.thumbnail = thumbnail;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

}
