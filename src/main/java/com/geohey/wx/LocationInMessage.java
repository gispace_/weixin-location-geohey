package com.geohey.wx;

import org.jdom.Element;

import com.geohey.utils.WXMessageUtils;

public class LocationInMessage extends WeiXinMessage{

	protected String fromUserName;
	private double x;
	private double y;
	private String createTime;
	
	public LocationInMessage() {
	}
	
	public static LocationInMessage parse(Element root){
		LocationInMessage inMessage = new LocationInMessage();
		String value = root.getChildText(WXMessageUtils.FROM_USER_NAME).trim();
		inMessage.setFromUserName(value);
		value = root.getChildText(WXMessageUtils.X).trim();
		double number = Double.parseDouble(value);
		inMessage.setX(number);
		value = root.getChildText(WXMessageUtils.Y).trim();
		number = Double.parseDouble(value);
		inMessage.setY(number);
		value = root.getChildText(WXMessageUtils.CREATE_TIME).trim();
		inMessage.setCreateTime(value);
		return inMessage;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
}
