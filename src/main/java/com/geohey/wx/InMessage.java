package com.geohey.wx;

import org.jdom.Element;

import com.geohey.utils.WXMessageUtils;

public class InMessage extends WeiXinMessage {

	public InMessage() {
	}

	public static InMessage parse(Element root) {
		InMessage inMessage = new InMessage();
		String value = root.getChildText(WXMessageUtils.FROM_USER_NAME).trim();
		inMessage.setFromUserName(value);
		value = root.getChildText(WXMessageUtils.TO_USER_NAME).trim();
		inMessage.setToUserName(value);
		return inMessage;
	}

}
